This is a Russian profile for Drupal installation.

Follow the steps below:

1. Install Drupal
* Download and extract the latest version Drupal (for example, drupal-5.1)
* Create the directories 'sites/all/modules' and 'sites/all/themes'

2. Copy Russian Installator
* Download Russian Installator archive and extract onto the 'profiles/' directory
* Rename the file 'ru.po.txt' to 'ru.po'

3. The autolocale module
* Download and extract autolocale module archive from 'http://drupal.org/project/autolocale' 
* Copy the autolocale directory onto 'sites/all/modules'
* Rename the file 'autolocale.ru.po.txt' to 'autolocale.ru.po'
* Copy the file autolocale.ru.po onto 'sites/all/modules/autolocale/po/autolocale.ru.po'

4. Russian modules translation
* Download and extract latest Russian modules translation from 'http://drupal.ru/node/3610'
(for example, now it is 'http://drupal.ru/files/ru_drupal5x_modules_v3.zip')
* Copy the files *.po from the translation onto 'sites/all/modules/autolocale/po/'

5. Start 'http://your_site' in your browser