<?php
/**
 * Russian Drupal installation profile 
 * ���᪨� ��䨫� ���⠫��樨 Drupal
 * http://drupal.org/project/RussianInstaller
 * http://drupal.ru/node/4195 (in Russian)
 */

// ������稬 ������⥪� Create/Rename/Update/Delete
include_once('profiles/crud.inc');


/**
 * Return an array of the modules to be enabled when this profile is installed.
 * ���᮪ ���㫥�, ����� ������ ���� ����祭� �� ���⠫��樨.
 *
 * @return
 *  An array of modules to be enabled.
 */
function russian_profile_modules() {
  // Core modules 
  // ���㫨 �� (���筮 �㦭� ��, �� ������� ����� �몫����: �mment, help ...)
  $core = array('block', 'color', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'taxonomy', 'user', 'watchdog');

  $core_app = array('profile', 'locale');

  // Contrib modules 
  // �������⥫�� ���㫨 (����� ��������� ��� 㤠���� �� ����室�����)
  $contrib = array('autolocale', 'simplemenu');

  return array_merge($core, $core_app, $contrib);
}

/**
 * Return a description of the profile for the initial installation screen.
 * ���ᠭ�� ��䨫� ��� �뢮�� �� ��砫쭮� ��࠭�� ���⠫��樨.
 * ���ਬ��, "�롥�� ��� ��䨫� ��� ��⠭���� ���᪮�� Drupal."
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function russian_profile_details() {
  return array(
    'name' => 'Russian Drupal',
    'description' => st('Select this profile for installation of Russian Drupal.')
  );
}


/**
 * Uses functionality in autolocale.install to import PO files
 * �ᯮ���� �㭪樮���쭮��� autolocale.install ��� ������ po-䠩���
 */
function russian_install() {
  _autolocale_install_po_files();
}


/**
 * Perform any final installation tasks for this profile.
 * �����, ����� ������ ���� ���⠫����. 
 * �����頥� ⥪�� ᮮ�饭��, ���஥ ����� �뢥�� ���짮��⥫� � �����襭�� ���⠫��樨.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function russian_profile_final() {
  // Insert default user-defined node types into the database.
  // �������� ⨯� ���ਠ��� Page (��࠭��) � Story (����⪠).
  $common = array(
    'module' => 'node',
    'custom' => TRUE,
    'modified' => TRUE,
    'locked' => FALSE,
    'has_body' => TRUE,
    'body_label' => st('Body'),
    'has_title' => TRUE,
    'title_label' => st('Title'),
  );
  $types = array(
    array_merge(
      array(
        'type' => 'page',
        'name' => st('Page'),
        'description' => st('If you want to add a static page, like a contact page or an about page, use a page.')
      ), 
      $common
    ),
    array_merge(
      array(
        'type' => 'story',
        'name' => st('Story'),
        'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.')
      ),
      $common
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  // ��࠭�� �� 㬮�砭��: �� �㡫������ �� ������� � �⪫�祭� �������ਨ.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  // ��࠭�� �� 㬮�砭��: �� �����뢠�� ���ଠ�� � ��� � ����.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);

//-- ����� �����稢����� �⠭����� ��䨫� default.profile

  // ������� �������⥫�� ����ன�� Russian Drupal Installer (RDI)
  RDI_add_config();

//return '';
}



/** 
 * Additional configuration 
 * �������⥫쭠� ���䨣����
 *
 */
function RDI_add_config() {
// ��ଫ����
// ---------- 

  // Sitename 
  // �������� ᠩ�, ���ਬ��, "���᪨� Drupal" (����஢�� UTF-8)
  variable_set('site_name', 'Русский Drupal');


  // Let's promote RDI
  // ����ன�� "�������" ��࠭��� � ����ࠩ⠬� � ४�����
  variable_set('site_footer', '&copy; 2007 <a href="http://www.drupal.ru" title="Russian Drupal">Русский Drupal</a>. Создано на <a href="http://www.drupal.org" title="Drupal, the best Open Source CMS">CMS Drupal</a> с помощью <a href="http://drupal.org/project/RussianInstaller" title="Russian Drupal Installer">Русского инсталлятора</a>.');


  // Turn on user pictures 
  // ����稬 ������ ���짮��⥫��
  variable_set('user_pictures', TRUE);


// ���짮��⥫�
// ------------ 

  // Register site admin 
  // �������� �������� ������ ᠩ� � ����� ��� �� �ࠢ�
        // ���: admin, ��஫�: admin, ��.����: admin@mydrupalsite.ru
        // ��������!!! �� ������ �������� �� ����ன�� �� ࠡ�祬 ᠩ� !!!
  db_query("INSERT INTO {users} (uid, name, pass, mail, created, status) VALUES(1, 'admin', '%s', 'admin@mydrupalsite.ru', %d, 1)", md5('admin'), time());
  user_authenticate('admin', 'admin');


/*
  // Setup some standard roles (non-uid-1 admin, redactor (content admin) etc.),
  // and configure all perms appropriately
  // ��������  ��᪮�쪮 �⠭������ ஫�� (����� ᠩ�, ।���� (�ࠢ����� ᮤ�ঠ����) � �.�.)
  // � ����� ᮮ⢥�����騥 �ࠢ� ����㯠
        //�ਬ��� �������� ஫�� (eng/rus/rus UTF-8):
        //redactor ।���� редактор
        //administrator ����������� администратор
        //moderator ������� модератор
        //author ���� автор
  db_query("INSERT INTO {role} (rid, name) VALUES (3, 'администратор')");
  db_query("INSERT INTO {role} (rid, name) VALUES (4, 'редактор')");

  // Insert new role's permissions 
  // ��।���� �ࠢ� ��� ஫��
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (3, 'administer blocks, use PHP for block visibility, access comments, administer comments, post comments, post comments without approval, access devel information, execute php code, devel_node_access module, view devel_node_access information, administer filters, administer menu, access content, administer content types, administer nodes, create page content, create story content, edit own page content, edit own story content, edit page content, edit story content, revert revisions, view revisions, access administration pages, administer site configuration, select different theme, administer taxonomy, access user profiles, administer access control, administer users, change own username', 0)");
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (4, 'administer blocks, access comments, administer comments, post comments, post comments without approval, administer menu, access content, administer nodes, create page content, create story content, edit own page content, edit own story content, edit page content, edit story content, revert revisions, view revisions, access user profiles, administer users', 0)");
*/


// ���� � �ࠢ� ��� ���
//
  // Set default role IDs
  $role_id['anonymous user'] = 1;
  $role_id['authenticated user'] = 2;
 
  // Create a Site Admin and Contributor role
  // a���������� администратор
  // ।���� редактор
  $role_id['Site Admin'] = install_add_role('администратор');
  $role_id['Contributor'] = install_add_role('редактор');
 
  // Set permissions for all roles
  $permissions['anonymous user'] = array(
    'access content',
    'access comments',
    'access site-wide contact form',
    'search content',
    'use advanced search',
    'access tinymce',
    'view uploaded files',
    'access user profiles'
  );
 
  // Authenticated is the same as anonymous, plus comment posting
  $permissions['authenticated user'] =
    array_merge( $permissions['anonymous user'],
      array(
        'post comments',
        'post comments without approval',
	'use original size',
	'access tinymce',
	'access all images',
	'access img_assist'
      )
    );
 
  // Contributor automatically has authenticated user role
  $permissions['Contributor'] = array(
    'edit own blog',
    'create story content',
    'edit story content',
    'create page content',
    'edit page content',
    'access tinymce',
    'create images',	
	'edit own images',	
	'view original images',
    'revert revisions',
    'view revisions',
    'view post access counter',
    'upload files',
    'post with no akismet checking'
  );
	
	
  // Site Admin has all permissions
  $permissions['Site Admin'] =
    array_merge( $permissions['Contributor'],
      array(
        'administer akismet settings',
		'moderate spam in comments',	
		'moderate spam in nodes of type Blog entry',	
		'moderate spam in nodes of type Image',	
		'moderate spam in nodes of type Page',	
		'moderate spam in nodes of type Story',
		'administer blocks',	
	    'use PHP for block visibility',
        'administer comments',
        'administer filters',
        'administer menu',
        'administer content types',	
	    'administer nodes',
	    'administer url aliases',	
	    'create url aliases',
        'administer pathauto',
        'administer search',
        'administer simplemenu',
        'view simplemenu',
        'access statistics',	
	    'access administration pages',
        'administer site configuration',
        'select different theme',
        'administer taxonomy',
        'administer tinymce',
        'administer access control',
        'administer users',
        'change own username',
        'administer views',
	'access tinymce',
	'access advanced options'
      )
    );
 
  foreach ($permissions as $rolename => $perms) {
    install_set_permissions($role_id[$rolename], $perms);
  }


// ���ਠ��
// ---------

  // Create a Welcome story
  // �������� �ਢ���⢥���� ��࠭���
  $welcome_body = "Добро пожаловать! This is a story to welcome you. Once you've created a new account or logged in with the account info you received via email, you'll be able to edit it.";
  
  $node = array('type' => 'story');
  $values = array(
    'title' => 'Добро пожаловать!',
    'body' => $welcome_body,
    'uid' => 1
  );

  // execute the form (equivalent to hitting submit) on the add/node/story page
  // ���������� ������ ������ '��ࠢ���' � �ଥ ����������/���ਠ�/����⪠
  $msg = drupal_execute('story_node_form', $values, $node);
  
  unset($node);
  unset($values);
    
  // Create an "About" page
  // �������� ��࠭��� "� ���"
  $about_body = 'О нас: This is an About page. Write what your site is about here.';
  
  $node = array('type' => 'page');
  $values = array(
    'title' => 'О нас',
    'body' => $about_body,
    'uid' => 1
  );
  
  $msg = drupal_execute('page_node_form', $values, $node);	
  

// ����
// ----

  // Create a Secondary links menu
  // ������� �������⥫쭮� ����
  $secondary_mid = install_menu_create_menu('Secondary links');
  variable_set('menu_secondary_menu', $secondary_mid);
  
  // Add Contact menu item to secondary links
  $contact_mid = install_menu_get_mid('contact');
  install_menu_set_menu($contact_mid, $secondary_mid);

  // Create menu entry for About page
  // �㭪� ���� ��� ��࠭��� '� ���' (О нас)
  $about_mid = install_menu_create_menu_item('node/2', 'О нас', $secondary_mid);
  

// ����ன�� SimpleMenu
  // Create an Admin menu for SimpleMenu
  // ������� ���� Admin ��� SimpleMenu
  $adminroot_mid = install_menu_create_menu('Admin');
  
  // Get the Administer menu ID
  // ����砥� �����䨪��� ���� Administer 
  $admin_mid = install_menu_get_mid('admin');
  
  // Set the Administer item to be underneath the new Admin menu, and disable it
  // ������ ���� Admin �㭪⮬ ���� Administer � �몫�砥� ���
  install_menu_set_menu($admin_mid, $adminroot_mid, 48);
  
  // Set SimpleMenu to use the Admin menu we just created
  // ����ࠨ����, �⮡� SimpleMenu �ᯮ�짮��� ���� Admin, ���஥ �� ᥩ�� ᮧ����
  variable_set('simplemenu_menu', $adminroot_mid);


// ���짮��⥫�᪨� ��䨫�
// ------------------------
    // Personal ��筮� Личное
  // Create some profile fields
  $fields = array(
    array(
      'category' => 'Личное',
      'title' => 'Full Name',
      'name' => 'fullname',
      'type' => 'textfield',
      'visibility' => 3,
      'register' => 1
    ),
    array(
      'category' => 'Личное',
      'title' => 'Website',
      'name' => 'website',
      'explanation' => 'Your personal website, blog, etc.',
      'type' => 'url',
      'visibility' => 3,
      'register' => 1
    ),
    array(
      'category' => 'Личное',
      'title' => 'Interests',
      'name' => 'interests',
      'explanation' => 'Keywords or tags that represent your interests',
      'type' => 'list',
      'visibility' => 3,
      'register' => 1
    ),
    array(
      'category' => 'Личное',
      'title' => 'About',
      'name' => 'about',
      'explanation' => 'Tell us a little about yourself, in the form of a biography or descriptive sentence or two.',
      'type' => 'textarea',
      'visibility' => 3,
      'register' => 1
    )
  );
 
  foreach ($fields as $field) {
    install_profile_field_add($field);
  }


// ����� ����ன��
// ----------------
  // Configurable timezones are confusing; turn them off
  // �⪫�稬 ����������� ����ன�� �ᮢ�� ���ᮢ ���짮��⥫ﬨ
//  variable_set('configurable_timezones', FALSE);
  
  // Set date and timezone settings
  // ���������� ����ன�� ���� � �६���
//  variable_set('date_format_medium', 'Y F j - g:i');
  variable_set('date_format_short', 'd/m/Y');
  variable_set('date_format_medium', 'd/m/Y - H:i');
  variable_set('date_format_long', 'j F Y, l - H:i');
  variable_set('date_first_day', '1'); // ����ᥭ� (0) ��� �������쭨� (1)


  // clean URLs work, so let's turn them on 
  // ����稬 ���� ��뫪�
  variable_set('clean_url', TRUE);


  // Set the default file paths 
  // ����ன�� 䠩����� ��⥬�: ��� �ᯮ������ ��⠫��� ��� 䠩���
  variable_set('file_directory_path', 'sites/all/files');
  variable_set('file_directory_temp', '/tmp');
// �᫨ ���� �� ��⠫��� � �������� � ��⠫�� profiles, ���� ⠪:
//  variable_set('file_directory_path', conf_path().'/files');
//  variable_set('file_directory_temp', conf_path().'/tmp');


  // ��᫥ �����襭�� ���⠫��樨 ��३� �� ��� ��࠭���
//  drupal_goto('admin');
}



/**
 * �������⥫�� ����������. �ਬ��, ����� �� ����� ᤥ���� ����ன�� �� ��䨫� ���⠫��樨.
 *
 */
function for_example() {
  // Mimic system_themes_submit 
  // ��⠭���� ����� ⥬� ��ଫ���� �� 㬮�砭�� (���ਬ��, danger4k)
      // �� ������ ᪮��஢��� 䠩�� ⥬� � ��⠫�� 'sites/all/theme/���_⥬�' !
  system_theme_data(); // collect available themes ᮡ�ࠥ� ����㯭� ⥬�
  db_query("UPDATE {system} SET status = 0 WHERE type = 'theme'");
  $themes = array('garland','danger4k');
  foreach ($themes as $theme) {
      system_initialize_theme_blocks($theme);  
      db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", $theme);
  }
  variable_set('theme_default','danger4k');


  // Change front page 
  // ������� ������� ��࠭��� ᠩ�, ���ਬ�� �� "user/register", �.�. �� ��࠭��� ॣ����樨 ������
  // (������ ���筮� "node" ����� ��⮬ �� ��࠭�� admin/settings/site-information)
  variable_set('site_frontpage', 'user/register'); 


  // Remove default line break filter for the FULL HTML filter 
  // ������� 䨫��� ��७�� ��ப� �� �ଠ� ����� Full HTML
  db_query("DELETE FROM {filters} WHERE format = 3");


  // Expanded Filtered HTML tags 
  // ���᮪ ⥣�� HTML, ࠧ�襭��� � 䨫��� Filtered HTML
  variable_set('allowed_html_1',
  '<a> <em> <strong> <cite> <code> <ul> <ol> <li> <dl> <dt> <dd> <img> <table> <tr> <td> <th> <div> <span> <p> <br> <blockquote> <hr>');

 
  // Full text 
  // � 䨤�� �����뢠�� ���� ⥪��, � �� ⮫쪮 �����
  variable_set('feed_item_length', 'fulltext');


  // write errors to the log, not to screen
  // ᮮ�饭�� �� �訡��� �뢮���� ⮫쪮 � ��⥬�� ��ୠ�, �� �� ��࠭
  variable_set('error_level', FALSE);


  // Set the quality a bit higher 
  // ����⢮ ����ࠦ���� ᤥ���� ���� (admin/settings/image-toolkit)
  variable_set('image_jpeg_quality', 90);

 
  // Tags vocab for stories  
  // ������� ᫮���� tags ��� ⨯� ���ਠ��� ����⪠
  $tags = array('name' => 'tags',
                'tags' => 1,
                'description' => '',
                'module' => 'taxonomy',
                'nodes' => array('story' => 1));
  taxonomy_save_vocabulary($tags);
 

  // Blog: turn off promote 
  // ����: �� �����뢠�� �� �������
  variable_set('node_options_blog', array('status'));

}
